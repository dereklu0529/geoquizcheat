package com.example.brianthoms.geoquizcheat;

/**
 * Created by brianthoms on 1/8/2018.
 */

public class Question {

    private int mTextResId;
    private boolean mAnswerTrue;
    private final int score=10;

    public int Score(){
        return this.score;
    }

    public int getTextResId() {
        return mTextResId;
    }

    public void setTextResId(int textResId) {
        mTextResId = textResId;
    }

    public boolean isAnswerTrue() {
        return mAnswerTrue;
    }

    public void setAnswerTrue(boolean answerTrue) {
        mAnswerTrue = answerTrue;
    }

    public Question(int textResId, boolean answerTrue) {
        mTextResId = textResId;
        mAnswerTrue = answerTrue;
    }
}
