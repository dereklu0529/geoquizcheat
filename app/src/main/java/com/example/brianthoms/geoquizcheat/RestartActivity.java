package com.example.brianthoms.geoquizcheat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class RestartActivity extends AppCompatActivity {

    private TextView mScoreTextView;
    private Button mRestartButton;
    private final static int REQUEST_CODE_RESTART=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restart);

        Intent fromQuizIntent = getIntent();
        int score = fromQuizIntent.getIntExtra("SCORE_EXTRA",0);

        mScoreTextView = (TextView) findViewById(R.id.mScoreTextView);
        mScoreTextView.setText(String.format((getResources().getString(R.string.score)),score));

        mRestartButton = (Button) findViewById(R.id.mRestartButton);
        mRestartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent r_intent= getIntent();
                r_intent.putExtra("KEY_INDEX",0);
                r_intent.putExtra("SCORE_EXTRA",0);
                setResult(RESULT_OK,r_intent);
                finish();
            }
        });
    }
}
